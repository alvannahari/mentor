-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 15, 2020 at 05:02 AM
-- Server version: 10.1.45-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krakatio_mentor`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `first_name`, `last_name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin@user.com', 'Admin', 'User', '$2y$10$rHL1xKiUP2eJ0UdbRnF8D.ayZ2G5QisbJU4zilL1ViV1.z8ZU.Lxu', '2020-05-22 08:50:01', '2020-05-22 08:50:01');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content_group_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `superuser_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Pdf','Video') COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `content_group_id`, `superuser_id`, `name`, `description`, `type`, `url`, `created_at`, `updated_at`) VALUES
(1, '1', '1', 'Coba youtube', '-', 'Video', 'https://www.youtube.com/watch?v=PqXaiAcZllc', '2020-07-08 10:13:11', '2020-07-08 10:13:11'),
(2, '2', '1', 'test konten', 'deskripsi test konten', 'Video', 'google.com', '2020-06-18 16:12:23', '2020-06-18 16:12:23'),
(3, '2', '1', 'Materi Sejarah', 'Deskripsi untuk Materi Sejarah', 'Pdf', '3_Pembelajaran Materi IPS_2020-06-25_03-07-35.pdf', '2020-05-22 08:50:01', '2020-05-22 08:50:01'),
(4, '2', '1', 'Materi Sejarah', 'Deskripsi untuk Materi Sejarah', 'Video', 'Sejarah.com', '2020-05-22 08:50:01', '2020-05-22 08:50:01'),
(5, '5', '1', 'Contoh Konten Agama ke 1', 'Deskripsi Contoh Konten Agama ke 1', 'Pdf', '17_konten_Contoh Konten Agama ke 1_2020-06-25_03-07-35.pdf', '2020-06-24 20:07:35', '2020-06-24 20:07:35'),
(18, '5', '1', 'Contoh Konten Agama ke 2', 'Deskripsi untuk Contoh Konten Agama ke 2', 'Video', 'google.com', '2020-06-24 20:08:13', '2020-06-24 20:08:13'),
(20, '1', '1', 'deri', 'tete', 'Pdf', '20_konten_deri_2020-06-25_04-26-55.pdf', '2020-06-24 21:26:55', '2020-06-24 21:26:55'),
(21, '5', '1', 'Test Content 1sdsdas', 'deskrisi', 'Pdf', '21_konten_Test Content 1_2020-06-25_19-34-28.pdf', '2020-06-25 12:34:28', '2020-06-25 12:49:33'),
(26, '9', '1', 'Tambah Konten Test', 'Deskripsi Tambah Konten Test', 'Video', 'youtube.com/maarif', '2020-07-09 00:45:37', '2020-07-09 00:45:37'),
(27, '1', '1', 'Coba PDF', 'aa', 'Pdf', '27_konten_Coba PDF_2020-07-10_11-33-06.pdf', '2020-07-10 04:33:06', '2020-07-10 04:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `content_groups`
--

CREATE TABLE `content_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `superuser_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_groups`
--

INSERT INTO `content_groups` (`id`, `superuser_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, '1', 'Pembelajaran Materi IPA', 'Deskripsi Untuk Pembelajran Materi IPAssss', '2020-05-21 08:50:01', '2020-06-15 13:07:21'),
(2, '2', 'Pembelajaran Materi IPS', 'Deskripsi Untuk Pembelajran Materi IPS', '2020-05-22 08:50:01', '2020-05-22 08:50:01'),
(5, '1', 'Pembelajaran Agamaa', 'Deskripsi Untuk Pembelajaran Agama', '2020-06-24 20:06:17', '2020-06-24 20:06:31'),
(9, '1', 'pembelajaran Bahasa', 'Deskripsi Untuk Pembeajaran BAhasa', '2020-07-09 00:45:03', '2020-07-09 00:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_05_12_152406_create_superusers', 1),
(10, '2020_05_12_153056_create_contents_table', 1),
(11, '2020_05_12_153109_create_content_groups_table', 1),
(12, '2020_05_12_153121_create_user_content_histories_table', 1),
(13, '2020_05_12_153137_create_user_content_comments_table', 1),
(14, '2020_05_12_153204_create_user_content_groups_table', 1),
(15, '2020_05_12_160113_create_admins_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('037ba603568652d5234b07c4c367f8c8811ca2b4d6fbf1ec236ae589520f4368d758ffdb41e73631', 1, 1, 'login', '[]', 0, '2020-07-05 20:00:27', '2020-07-05 20:00:27', '2021-07-06 03:00:27'),
('08b69db84f6734c0cba0b6ed89bfff168c27ebd07d4b13a55c7e44636639ae043417944dadd36d3b', 8, 1, 'register', '[]', 0, '2020-06-24 19:28:22', '2020-06-24 19:28:22', '2021-06-25 02:28:22'),
('233ff7b73e23222354ef7f3fccdd57ae30617f34972854501fa238bd169137a1383a452aa5d53319', 5, 1, 'login', '[]', 0, '2020-06-18 15:07:39', '2020-06-18 15:07:39', '2021-06-18 22:07:39'),
('4befcf949476e6a9f1b65fbcf4dafc3e5828f1880fc40278da0599dd5895f00cd7629d37ea35a0ff', 1, 1, 'login', '[]', 0, '2020-07-13 18:03:49', '2020-07-13 18:03:49', '2021-07-14 01:03:49'),
('54d865421ea7b186b27dde1c97b4e886dbff80ec240ad63d62451b427bf1ca9657f5c165b02a92d4', 9, 1, 'login', '[]', 0, '2020-06-24 21:30:46', '2020-06-24 21:30:46', '2021-06-25 04:30:46'),
('87dbeca5cba48b63d1033fd6171d681bf0c56c4a1985a870f15b7f2a2931f95bdc95f0a45e55565c', 1, 1, 'login', '[]', 0, '2020-07-13 18:04:15', '2020-07-13 18:04:15', '2021-07-14 01:04:15'),
('a0d10cfbeee45efcb50fbb090aa2470d11879bf9e2fbe09724fb247dac1475e68d83dbc4ff978882', 5, 1, 'login', '[]', 0, '2020-06-24 15:27:28', '2020-06-24 15:27:28', '2021-06-24 22:27:28'),
('abb530b3909fe05d7256a998e8123d9e7d9539a53966c5d2f6e9f563073db2772ca8932eb40e5033', 1, 1, 'login', '[]', 0, '2020-07-07 19:14:01', '2020-07-07 19:14:01', '2021-07-08 02:14:01'),
('c8bc580b6168376b9409375d282857611808bca3323b6005b202334ebb70eec0798241c112a26cd1', 1, 1, 'login', '[]', 0, '2020-07-10 06:30:43', '2020-07-10 06:30:43', '2021-07-10 13:30:43'),
('df71b3d197b1f8f9a67d7b339201ba4990813a89b7302dac21620b49d2f3b3f9f0588c5ac6313045', 10, 1, 'register', '[]', 0, '2020-06-24 21:31:36', '2020-06-24 21:31:36', '2021-06-25 04:31:36'),
('ed00581335e36668df3a4336b2474d83fe58c3b01c02a4c6cd8cb0e001ab80b61884e03fc8a63f08', 9, 1, 'register', '[]', 0, '2020-07-05 20:01:00', '2020-07-05 20:01:00', '2021-07-06 03:01:00'),
('fc91a38793808dea6e1d7f3f30e5ec7e6ed7e74039814f270d799e38791b482a3efde09fb2361e6b', 1, 1, 'login', '[]', 0, '2020-07-13 17:41:32', '2020-07-13 17:41:32', '2021-07-14 00:41:32');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'oO25gRoVnwxSC4RRHyGr3LSzaA628hCyPGZOiBts', 'http://localhost', 1, 0, 0, '2020-06-18 15:07:02', '2020-06-18 15:07:02'),
(2, NULL, 'Laravel Password Grant Client', 'coiNQPVT52bxgI9Hts64ouEaiP3NV8NWQVnS2fT7', 'http://localhost', 0, 1, 0, '2020-06-18 15:07:03', '2020-06-18 15:07:03');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-18 15:07:03', '2020-06-18 15:07:03');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `superusers`
--

CREATE TABLE `superusers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `superusers`
--

INSERT INTO `superusers` (`id`, `username`, `first_name`, `last_name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'super@user.com', 'Super', 'User', '$2y$10$oYRt2L9LML3QmJyACffI/Otuf80otmbjWBIZraejMJwbV5jYSxg4u', '2020-05-22 08:50:00', '2020-05-22 08:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('Laki-laki','Perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `last_name`, `phone_number`, `gender`, `password`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'user01@gmail.com', 'user', '01', '085631948069', 'Perempuan', '$2y$10$uSysVKtGXWCHDqVrE4BOlugJXrDJO6sSY0Nec40qT3JaeRHoo9TJy', NULL, NULL, '2020-06-24 21:30:34', '2020-06-24 21:33:04'),
(2, 'user02@gmail.com', 'user', '02', '08598765432', 'Laki-laki', '$2y$10$WjzcgBN13LvGRuFJkeOTH.F7C/vU6DuErXymCFWl6xyKEiwuk4Rnm', NULL, NULL, '2020-06-18 15:05:36', '2020-06-18 15:05:36'),
(3, 'deripotato@gmail.com', 'deri', 'armanda', '081244543534', 'Laki-laki', '$2y$10$wgB8mi51GnDu0E0AYsNqc.gULkutWaqCE9IqURsucHf0C2NdZv5.m', NULL, NULL, '2020-05-22 08:50:00', '2020-06-18 13:55:09'),
(4, 'user04@gmail.com', 'user', '04', '0857452208', 'Perempuan', '$2y$10$hBBasUPYmAvhcuXN.m7hxugVN9EH1PXn6DSbJgl9XpnCnjyd4cz.m', NULL, NULL, '2020-06-24 21:31:36', '2020-06-24 21:31:36'),
(5, 'user05@gmail.com', 'user', '05', '08576353654776', 'Perempuan', '$2y$10$8DRBKHi09uqHV3baf0nsK.ZuNdZR.dQSMRYKv1Qf81dukmNTpAgh.', NULL, NULL, '2020-06-18 15:05:36', '2020-06-24 19:27:16');

-- --------------------------------------------------------

--
-- Table structure for table `user_content_comments`
--

CREATE TABLE `user_content_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_content_comments`
--

INSERT INTO `user_content_comments` (`id`, `user_id`, `content_id`, `comment`, `created_at`, `updated_at`) VALUES
(3, '1', '1', 'Komen dari User 1 Untuk Konten 1', '2020-05-22 13:57:46', '2020-05-22 13:57:46'),
(4, '1', '2', 'Komen dari User 1 Untuk Konten 2', '2020-05-24 13:57:46', '2020-05-25 13:57:46'),
(5, '0', '2', 'tambah komentar', '2020-06-15 15:38:04', '2020-06-15 15:38:04'),
(8, '1', '1', 'Komen Lagi dari User 1 Untuk Konten 1', '2020-05-26 13:57:46', '2020-05-26 13:57:46'),
(9, '1', '2', 'Komen Lagi dari User 1 Untuk Konten 2', '2020-05-27 13:57:46', '2020-05-27 13:57:46'),
(10, '5', '1', 'kirim komentar lewat postman', '2020-06-24 19:56:13', '2020-06-24 19:56:13'),
(11, '0', '18', 'komentar 1 Dari Admin untuk konten 18', '2020-06-24 20:10:56', '2020-06-24 20:10:56'),
(12, '0', '18', 'Komentar 2 Dari Admin Untuk Konten 18', '2020-06-24 20:11:21', '2020-06-24 20:11:21'),
(13, '1', '5', 'kirim komentar lewat postman', '2020-06-24 21:44:02', '2020-06-24 21:44:02'),
(14, '1', '1', 'Sdfgg', '2020-07-08 12:56:10', '2020-07-08 12:56:10'),
(15, '1', '21', 'Assx', '2020-07-08 17:35:55', '2020-07-08 17:35:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_content_groups`
--

CREATE TABLE `user_content_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_group_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_content_groups`
--

INSERT INTO `user_content_groups` (`id`, `user_id`, `content_group_id`, `created`) VALUES
(6, '5', '2', '2020-05-22 09:35:40'),
(7, '1', '2', '2020-05-22 14:37:59'),
(8, '1', '1', '2020-05-27 07:20:58'),
(9, '5', '1', '2020-05-27 07:20:58'),
(10, '1', '4', '2020-06-18 13:23:51'),
(11, '1', '5', '2020-06-24 20:06:51'),
(12, '5', '5', '2020-06-24 20:06:51'),
(13, '8', '1', '2020-06-24 20:12:39'),
(14, '2', '1', '2020-07-08 10:35:09'),
(15, '3', '1', '2020-07-08 10:35:09'),
(16, '4', '1', '2020-07-08 10:35:09'),
(17, '9', '1', '2020-07-08 10:35:09'),
(18, '2', '5', '2020-07-08 20:24:11'),
(19, '3', '5', '2020-07-08 20:24:11'),
(20, '4', '5', '2020-07-08 20:24:11'),
(21, '1', '9', '2020-07-09 00:45:13'),
(22, '2', '9', '2020-07-09 00:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `user_content_histories`
--

CREATE TABLE `user_content_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_content_histories`
--

INSERT INTO `user_content_histories` (`id`, `user_id`, `content_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '2020-05-21 17:00:00', '2020-05-22 13:57:46'),
(2, '1', '3', '2020-05-24 17:00:00', '2020-05-22 13:57:43'),
(3, '2', '1', '2020-05-24 17:00:00', '2020-05-21 13:57:46'),
(4, '2', '2', '2020-05-29 17:00:00', '2020-05-22 13:57:46'),
(5, '5', '1', '2020-06-24 19:29:50', '2020-06-24 19:29:50'),
(6, '1', '20', '2020-07-08 12:57:06', '2020-07-08 12:57:06'),
(7, '1', '3', '2020-07-08 16:28:36', '2020-07-08 16:28:36'),
(8, '1', '21', '2020-07-08 17:32:58', '2020-07-08 17:32:58'),
(9, '1', '20', '2020-07-08 17:34:29', '2020-07-08 17:34:29'),
(10, '1', '21', '2020-07-08 17:35:26', '2020-07-08 17:35:26'),
(11, '1', '21', '2020-07-08 17:36:57', '2020-07-08 17:36:57'),
(12, '1', '3', '2020-07-10 06:33:38', '2020-07-10 06:44:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_groups`
--
ALTER TABLE `content_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `superusers`
--
ALTER TABLE `superusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_content_comments`
--
ALTER TABLE `user_content_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_content_groups`
--
ALTER TABLE `user_content_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_content_histories`
--
ALTER TABLE `user_content_histories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `content_groups`
--
ALTER TABLE `content_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `superusers`
--
ALTER TABLE `superusers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_content_comments`
--
ALTER TABLE `user_content_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_content_groups`
--
ALTER TABLE `user_content_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_content_histories`
--
ALTER TABLE `user_content_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
