<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(UsersTableSeeder::class);
        // $this->call(SuperusersTableSeeder::class);
        // $this->call(AdminsTableSeeder::class);
        // $this->call(ContentsTableSeeder::class);
        // $this->call(CommentUsersTableSeeder::class);
        // $this->call(HistoryUsersTableSeeder::class);
    }
}
