<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperusersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('superusers')->insert([
            'username'          => 'super@user.com',
            'password'          => bcrypt('superuser'),
            'first_name'        => 'Super',
            'last_name'         => 'User',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
    }
}
