<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'email'             => 'user01@gmail.com',
            'password'          => bcrypt('user01'),
            'phone_number'      => '085123456789',
            'first_name'        => 'user',
            'last_name'         => '01',
            'gender'            => 'Laki-laki',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        // DB::table('users')->insert([
        //     'email'             => 'user02@gmail.com',
        //     'password'          => bcrypt('user02'),
        //     'phone_number'      => '08598765432',
        //     'first_name'        => 'user',
        //     'last_name'         => '02',
        //     'gender'            => 'Laki-laki',
        //     'created_at'        => now(),
        //     'updated_at'        => now()
        // ]);
    }
}
