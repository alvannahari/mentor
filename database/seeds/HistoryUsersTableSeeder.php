<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistoryUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('user_content_histories')->insert([
            'user_id'           => 1,
            'content_id'        => 1,
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('user_content_histories')->insert([
            'user_id'           => 1,
            'content_id'        => 2,
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('user_content_histories')->insert([
            'user_id'           => 2,
            'content_id'        => 1,
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('user_content_histories')->insert([
            'user_id'           => 2,
            'content_id'        => 2,
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
    }
}
