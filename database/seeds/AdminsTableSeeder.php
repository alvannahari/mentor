<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('admins')->insert([
            'username'          => 'admin@user.com',
            'password'          => bcrypt('adminuser'),
            'first_name'        => 'Admin',
            'last_name'         => 'User',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
    }
}
