<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Seeder for User Content Group
        DB::table('user_content_groups')->insert([
            'user_id'           => 1,
            'content_group_id'  => 1
        ]);
        DB::table('user_content_groups')->insert([
            'user_id'           => 1,
            'content_group_id'  => 2
        ]);
        DB::table('user_content_groups')->insert([
            'user_id'           => 2,
            'content_group_id'  => 2
        ]);
        // DB::table('user_content_groups')->insert([
        //     'user_id'           => 2,
        //     'content_group_id'  => 2,
        //     'created_at'        => now(),
        //     'updated_at'        => now()
        // ]);

        // Seeder for Content Group
        DB::table('content_groups')->insert([
            'superuser_id'      => 1,
            'name'              => 'Pembelajaran Materi IPA',
            'description'       => 'Deskripsi Untuk Pembelajran Materi IPA',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('content_groups')->insert([
            'superuser_id'      => 2,
            'name'              => 'Pembelajaran Materi IPS',
            'description'       => 'Deskripsi Untuk Pembelajran Materi IPA',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);

        // Seeder for Content
        DB::table('contents')->insert([
            'content_group_id'  => 1,
            'superuser_id'      => 1,
            'name'              => 'Materi Biologi',
            'description'       => 'Deskripsi Untuk Materi Biologi',
            'type'              => 'Pdf',
            'url'               => 'Biologi.com',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('contents')->insert([
            'content_group_id'  => 1,
            'superuser_id'      => 1,
            'name'              => 'Materi Biologi',
            'description'       => 'Deskripsi Untuk Materi Biologi',
            'type'              => 'Video',
            'url'               => 'Biologi.com',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('contents')->insert([
            'content_group_id'  => 2,
            'superuser_id'      => 1,
            'name'              => 'Materi Sejarah',
            'description'       => 'Deskripsi untuk Materi Sejarah',
            'type'              => 'Pdf',
            'url'               => 'Sejarah.com',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('contents')->insert([
            'content_group_id'  => 2,
            'superuser_id'      => 1,
            'name'              => 'Materi Sejarah',
            'description'       => 'Deskripsi untuk Materi Sejarah',
            'type'              => 'Video',
            'url'               => 'Sejarah.com',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
    }
}
