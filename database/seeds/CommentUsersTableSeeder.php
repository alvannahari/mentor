<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('user_content_comments')->insert([
            'user_id'           => 1,
            'content_id'        => 1,
            'comment'           => 'Komen dari User 1 Untuk Konten 1',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('user_content_comments')->insert([
            'user_id'           => 1,
            'content_id'        => 2,
            'comment'           => 'Komen dari User 1 Untuk Konten 2',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('user_content_comments')->insert([
            'user_id'           => 2,
            'content_id'        => 1,
            'comment'           => 'Komen dari User 2 Untuk Konten 1',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
        DB::table('user_content_comments')->insert([
            'user_id'           => 2,
            'content_id'        => 2,
            'comment'           => 'Komen dari User 2 Untuk Konten 2',
            'created_at'        => now(),
            'updated_at'        => now()
        ]);
    }
}
