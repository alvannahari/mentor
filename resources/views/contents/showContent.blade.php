@extends('template.layout')

@section('title')
    Detail Konten
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="card">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Detail Konten </span>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-sm btn-danger" style="float: right" data-toggle="modal" data-target="#modal-delete"> <i class="mdi mdi-delete"></i> Delete</button>
                        <button class="btn btn-sm btn-warning m-r-15" style="float: right" data-toggle="modal" data-target="#modal-update-content"> <i class="mdi mdi-update"></i> Ubah</button>
                    </div>
                </div>
            </span>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label>Nama Kategori</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['name'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Deskripsi</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['description'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Type</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['type'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label style="margin-top: 4px">Preview</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center;margin-top: 4px"> : </div>
                                <div class="col-11"><a href="{{ $content['type'] == 'Pdf' ? asset('assets/file_content').'/'.$content['url'] : 'http://'.$content['url'] }}" target="_blank" class="btn btn-sm btn-primary"><i class="mdi mdi-eye"></i> Lihat File</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Tanggal Dibuat</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['created_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Terakhir Diperbarui</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['updated_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="card" style="height: 369px;">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Riwayat Pengguna </span>
                    </div>
                    {{-- <div class="col-6">
                        <button class="btn btn-sm btn-warning" style="float: right" data-toggle="modal" data-target="#modal-add-user"> <i class="mdi mdi-update"></i> Ubah</button>
                    </div> --}}
                </div>
            </span>
            <div class="card-body pt-0">
                <table class="table table-hover table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr style="line-height: 2.2">
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA</th>
                            <th style="border-top: none;">EMAIL</th>
                            <th style="border-top: none;"><center>TANGGAL</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($content['history'] as $history)
                            <tr>
                                <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                <td>{{ $history['user']['first_name'].' '.$history['user']['last_name'] }}</td>
                                <td>{{ $history['user']['email'] }}</td>
                                <td><center>{{ $history['created_at'] }}</center></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Belum Ada Pengguna yang Membuka Konten Ini</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <span class="card-title p-3"> 
        <div class="row">
            <div class="col-6">
                <span style="font-size: 18px;" >Forum Pembahasan Konten Ini </span>
            </div>
            <div class="col-6">
                <button class="btn btn-sm btn-primary" style="float: right" data-toggle="modal" data-target="#modal-add-comment"> <i class="mdi mdi-plus"></i> Tambah Komentar</button>
            </div>
        </div>
    </span>
    <div class="card-body pt-0" >
        <div class="container">
        <table class="comment table table-hover table-sm" width="100%" >
            {{-- <thead>
                <tr>
                    <th style="border-top: none;">NO.</th>
                    <th style="border-top: none;">NAMA</th>
                    <th style="border-top: none;">EMAIL</th>
                    <th style="border-top: none;">KOMENTAR</th>
                    <th style="border-top: none;"><center>TANGGAL</center></th>
                    <th style="border-top: none;"></th>
                </tr>
            </thead> --}}
            <tbody>
                @forelse ($content['comment'] as $comment)
                    <tr>
                        <td style="width: 40px;">{{ $loop->iteration }}</td>
                        <td style="text-align: left;">
                            @if ($comment['user_id'] == 0)
                                <p style="margin-bottom: 0.4rem"><b>admin</b> - admin_app@mentor.com</p>
                            @else
                                <p style="margin-bottom: 0.4rem"><b>{{ $comment['user']['first_name'].' '.$comment['user']['last_name'] }}</b> - {{ $comment['user']['email'] }}</p>
                            @endif
                            <p style="margin-bottom: 0.6rem;">
                                {{ $comment['comment'] }}
                            </p>
                        </td>
                        <td style="width: 180px;">{{ $comment['created_at'] }}</td>
                        <td style="width: 80px">
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteComment({{ $comment['id'] }})"><i class="mdi mdi-delete"></i> Delete</button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">Belum Ada Komentar Untuk Kontent Ini !</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
    </div>
</div>

{{-- Modal Update Content --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-content">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ubah Konten Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form class=" m-t-10" id="form-content">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nama Konten</label>
                            <input type="text" class="form-control" form="form-content" name="name" value="{{ $content['name'] }}"> 
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="2" form="form-content" name="description">{{ $content['description'] }}</textarea>
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        {{-- <div class="form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" disabled value="{{ $content['type'] }}">
                            <input type="hidden" form="form-content" name="type" value="{{ $content['type'] }}">
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        <div class="form-group">
                            <label>{{ $content['type'] == 'Pdf' ? 'File' : 'Alamat URL' }}</label>
                            <input type="{{ $content['type'] == 'Pdf' ? 'file' : 'text'}}" class="form-control" form="form-content" name="url" value="{{ $content['url'] }}">
                            <span class="help-block" style="float: right;color:red"></span>
                        </div> --}}
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-info" id="btn-submit-update-content">Simpan</button>
        </div>
        </div>
    </div>
</div>

{{-- Modal Add Comment --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-comment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Komentar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class=" m-t-10" id="form-comment">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Komentar</label>
                                <textarea class="form-control" rows="3" form="form-comment" name="comment"></textarea>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-info" id="btn-add-comment">Tambahkan</button>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete Content --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Konfirmasi Hapus</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Apakah Anda Yakin Ingin Menghapus Data Ini ? </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
        </div>
        </div>
    </div>
</div>

{{-- Modal Delete Comment --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-comment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Konfirmasi Hapus</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="input-delete-comment">
            <p>Apakah Anda Yakin Ingin Menghapus Data Ini ? </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-danger" id="btn-delete-comment">Hapus</button>
        </div>
        </div>
    </div>
</div>

<script>
    $('#btn-submit-update-content').click( function() {
        var button = $(this);
        emptyFormInput();
        $.ajax({
            type: "patch",
            url: "{{ url('content').'/'.$content['id'] }}",
            data: $('#form-content').serialize(),
            dataType: "json",
            beforeSend: function(response) {
                button.prop('disable', true);
                button.html('Menyimpan...');
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    for (var key of Object.keys(response.error)) {
                        $('#form-content [name="'+key+'"]').addClass('is-invalid');
                        $('#form-content [name="'+key+'"]').next().html(response.error[key]);
                        // console.log(key + " -> " + response.error[key])
                    }
                    alert('Data Kontent Gagal Disimpan !');
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    });

    $('#btn-delete').click( function(){
        var button = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('content').'/'.$content['id'].'/delete' }}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend: function (response) {
                button.prop('disable', true);
                button.html('Menyimpan...');
            },
            success: function (response) {
                if (!response.error) {
                    history.back();
                    // '{{ url()->previous() }}';
                } else {
                    alert(response.error);
                    button.prop('disable', false);
                    button.html('Simpan');
                }
            }
        });
    });

    $('#btn-add-comment').click( function() {
        var button = $(this);
        $.ajax({
            type: "post",
            url: "{{ url('comment').'/'.$content['id'] }}",
            data: $('#form-comment').serialize(),
            dataType: "json",
            beforeSend: function(response) {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            success: function (response) {
                if(!response.error) {
                    location.reload();
                } else {
                    $('[name=comment]').addClass('is-invalid');
                    $('[name=comment]').next().html('field description is required');
                    alert('Ada Beberapa Kolom yang Salah !')
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    })

    function deleteComment(id) {
        $('#modal-delete-comment').modal('show');
        $('#input-delete-comment').val(id);
        $('#btn-delete-comment').click( function(){
            var button = $(this);
            $.ajax({
                type: "get",
                url: "{{ url('comment').'/' }}"+$('#input-delete-comment').val()+"/delete",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: "json",
                beforeSend: function (response) {
                    button.html('Menghapus...');
                    button.prop('disable', true);
                },
                success: function (response) {
                    if (!response.error) {
                        location.reload();
                    } else {
                        alert(response.error)
                        button.html('Hapus');
                        button.prop('disable', false);
                    }
                }
            });
        })
    }
</script>

@endsection