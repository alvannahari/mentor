@extends('template.layout')

@section('title')
    Info Konten
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="card">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Info Konten </span>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-sm btn-danger" style="float: right" onclick="removeContent(0, {{ $content['id'] }})"> <i class="mdi mdi-delete"></i> Delete</button>
                        <button class="btn btn-sm btn-warning m-r-15" style="float: right" data-toggle="modal" data-target="#modal-update-group"> <i class="mdi mdi-update"></i> Ubah</button>
                    </div>
                </div>
            </span>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label>Nama Kategori</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['name'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Deskripsi</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['description'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Tanggal Dibuat</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['created_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Terakhir Diperbarui</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $content['updated_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="card" style="height: 283px;">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Akses Pengguna </span>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-sm btn-warning" style="float: right" data-toggle="modal" data-target="#modal-add-user"> <i class="mdi mdi-update"></i> Ubah</button>
                    </div>
                </div>
            </span>
            <div class="card-body pt-0">
                <div style="width:100%;display: block;height: 200px;overflow-y: scroll;">
                <table class="table table-hover table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr style="line-height: 2.2">
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA</th>
                            <th style="border-top: none;">EMAIL</th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($content['users'] as $user)
                            <tr>
                                <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                <td>{{ $user['first_name'].' '.$user['last_name'] }}</td>
                                <td>{{ $user['email'] }}</td>
                                <td style="width: 80px">
                                    <a href="{{ url('/user').'/'.$user['id'] }}" class="btn btn-info btn-sm" data-placement="top" data-original-title="Profil"><i class="mdi mdi-account"></i> Profil</a>
                                    {{-- <button class="btn btn-sm btn-danger m-l-5" onclick="removeUser({{ $user['id'] }})" ><i class="mdi mdi-delete"></i> Delete</button> --}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak Ada Akses Pengguna</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <span class="card-title p-3"> 
        <div class="row">
            <div class="col-6">
                <span style="font-size: 18px;" >Daftar Konten </span>
            </div>
            <div class="col-6">
                <button class="btn btn-sm btn-primary" style="float: right" data-toggle="modal" data-target="#modal-add-content"> <i class="mdi mdi-plus"></i> Tambah Konten</button>
            </div>
        </div>
    </span>
    <div class="card-body pt-0" >
        <div class="container">
        <table class=" table table-hover table-sm" width="100%" >
            <thead>
                <tr>
                    <th style="border-top: none;">NO.</th>
                    <th style="border-top: none;">NAMA</th>
                    <th style="border-top: none;">DESKRIPSI</th>
                    <th style="border-top: none;">TIPE</th>
                    <th style="border-top: none;"><center>TERAKHIR DIPERBARUI</center></th>
                    <th style="border-top: none;"></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($content['content'] as $contents)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $contents['name'] }}</td>
                        <td>{{ $contents['description'] }}</td>
                        <td>{{ $contents['type'] }}</td>
                        <td><center>{{ $contents['updated_at'] }}</center></td>
                        <td style="width: 165px">
                            <a href="{{ url('content').'/'.$contents['id'] }}" class="btn btn-sm btn-success" title="Detail"><i class="mdi mdi-magnify"></i> Detail</a>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="removeContent(1, {{ $contents['id'] }})"><i class="mdi mdi-delete"></i> Delete</button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">Konten Masih Kosong !</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
    </div>
</div>

{{-- Modal Update Contents --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-group">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ubah Data Kategori</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form class=" m-t-10" id="form-contents">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input class="form-control" rows="2" form="form-contents" name="name" value="{{ $content['name'] }}"> 
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="2" form="form-contents" name="description">{{ $content['description'] }}</textarea>
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-info" onclick="updateGroup()" id="btn-update-group">Simpan</button>
        </div>
        </div>
    </div>
</div>

{{-- Modal Add User Access --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Update Akses Pengguna</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form class=" m-t-10" id="form-access">
                <table class="table table-hover table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr style="line-height: 2.2">
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA</th>
                            <th style="border-top: none;">EMAIL</th>
                            <th style="border-top: none;">AKSES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @csrf
                        @forelse ($users as $user)
                            <tr>
                                <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                <td>{{ $user['first_name'].' '.$user['last_name'] }}</td>
                                <td>{{ $user['email'] }}</td>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <?php $checked = in_array($user['id'], $roles) ? true : false; ?>
                                        {{ Form::checkbox('user[]', $user['id'], $checked, ['class'=>'custom-control-input', 'form' => 'form-access' ,'id' => 'customCheck'.$loop->iteration]) }}
                                        <label class="custom-control-label" for="customCheck{{ $loop->iteration }}">Diberikan</label>
                                    </div>
                                    {{-- <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                                    </div> --}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak Ada Akses Pengguna</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-info" onclick="updateAccess()" id="btn-update-access">Simpan</button>
        </div>
        </div>
    </div>
</div>

{{-- Modal Add Content --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-content">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Tambah Konten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form class=" m-t-10" id="form-content">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nama Konten</label>
                            <input class="form-control" rows="2" form="form-content" name="name" > 
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="2" form="form-content" name="description"></textarea>
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        <div class="form-group">
                            <label>Tipe</label>
                            <select name="type" form="form-content" class="form-control">
                                <option selected>Pdf</option>
                                <option>Video</option>
                            </select>
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" class="form-control" form="form-content" name="url">
                            <span class="help-block" style="float: right;color:red"></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-info" id="btn-submit-add-content">Simpan</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Konfirmasi Hapus</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="input-delete">
            <p>Apakah Anda Yakin Ingin Menghapus Data Ini ? </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
        </div>
        </div>
    </div>
</div>

<script>
    $('select[name=type]').change(function(){
        $('input[name=url]').val('');
        var value = $(this).val();
        if (value == 'Pdf') {
            $('input[name=url]').prev().html('File');
            $('input[name=url]').attr('type', 'file');
        };
        if (value == 'Video') {
            $('input[name=url]').prev().html('URL Video');
            $('input[name=url]').attr('type', 'text');
        };
    })

    function updateGroup() {
        emptyFormInput();
        $.ajax({
            url: "{{ url('contents').'/'.$content['id'] }}",
            type: "patch",
            data: $('#form-contents').serialize(),
            dataType: "json",
            beforeSend: function(){
                $('#btn-update-group').html('Menyimpan Data...');
                $('#btn-update-group').prop('disable', true);
            },
            success : function(response){
                if (!response.error) {
                    location.reload();
                    // console.log(response.response);
                } else {
                    for (var key of Object.keys(response.error)) {
                        $('#form-contents [name="'+key+'"]').addClass('is-invalid');
                        $('#form-contents [name="'+key+'"]').next().html(response.error[key]);
                        // console.log(key + " -> " + response.error[key])
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                    $('#btn-update-group').html('Simpan');
                    $('#btn-update-group').prop('disable', false);
                }
            }
        });
    }

    function removeContent(temp, id) {
        $('#input-delete').val(id);
        $('#modal-delete').modal('show');
        $('#btn-delete').click(function() {
            if (temp  == 0) {
                var url = "{{ url('contents') }}/"+$('#input-delete').val()+"/delete"; 
            } else {
                var url = "{{ url('content') }}/"+$('#input-delete').val()+"/delete"; 
            };
            $.ajax({
                type: "get",
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: "json",
                beforeSend:function () {
                    $('#btn-delete').html('Menghapus ...');
                    $('#btn-delete').prop('disable', true);
                },
                success: function (response) {
                    if (!response.error) {
                        if (temp == 0) {
                            history.back();
                            // window.location = "/contents";
                        } else {
                            location.reload();
                        }
                    } else {
                        alert(response.error);
                        $('#btn-delete').html('Hapus');
                        $('#btn-delete').prop('disable', false);
                    }
                }
            });
        });
    }

    function updateAccess() {
        // console.log($('#form-access').serializeArray());
        $.ajax({
            type: "post",
            url: "{{ url('accessContents').'/'.$content['id'] }}",
            data: $('#form-access').serialize(),
            beforeSend: function () {
                $('#btn-update-access').html('Menyimpan...');
                $('#btn-update-access').prop('disable', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    $('#btn-update-access').html('Simpan');
                    $('#btn-update-access').prop('disable', false);
                }
                // console.log(response.data);
            }
        });
    }

    $('#btn-submit-add-content').click(function() {
        emptyFormInput();
        var button = $(this);
        var formData = new FormData($('#form-content')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('content').'/'.$content['id'] }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    for (var key of Object.keys(response.error)) {
                        $('#form-content [name="'+key+'"]').addClass('is-invalid');
                        $('#form-content [name="'+key+'"]').next().html(response.error[key]);
                        // console.log(key + " -> " + response.error[key])
                    }
                    alert('Data Kontent Gagal Disimpan !');
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            },
            complete: function() {
                button.html('Simpan');
                button.prop('disable', false);
            }
        });
    });

</script>
@endsection