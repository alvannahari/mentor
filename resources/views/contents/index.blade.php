@extends('template/layout')

@section('title', 'Konten')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh kategori yang akan ditampilkan untuk pengguna sesuai dengan kategori yang diizinkan. Data kategori termasuk kontent berbentuk pdf dan video pembelajaran</h6>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary m-b-10" data-toggle="modal" data-target="#modal-add-group" style="float: right;"><i class="mdi mdi-plus"></i> Tambah Kategori</button>
                    </div>
                </div>
                {{-- <h6 class="card-title m-t-40"><i class="m-r-5 font-18 mdi mdi-numeric-1-box-multiple-outline"></i> Table With Outside Padding</h6>  --}}
                <div class="table-responsive">
                    <table id="table-category" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>NAMA KATEGORI</th>
                                <th>DESKRIPSI</th>
                                <th style="text-align: center">TERDAFTAR</th>
                                <th style="text-align: center">TERAKHIR DIPERBARUI</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td style="width: 5px;text-align: center">2</td>
                                <td>Kategori Kedua</td>
                                <td>Deskripsi untuk Kategori Kedua</td>
                                <td style="text-align: center;width: 170px">21-04-2020 07:15:43</td>
                                <td style="text-align: center;width: 170px">25-04-2020 14:01:21</td>
                                <td style="width: 165px">
                                    <a href="#" class="btn btn-success btn-sm" data-placement="top" data-original-title="Detail"><i class="mdi mdi-magnify"></i> Detail</a>
                                    <button class="btn btn-sm btn-danger m-l-5" onclick="deleteGroup()" ><i class="mdi mdi-delete"></i> Delete</button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-group">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambahkan Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-t-10" id="form-category" >
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                @csrf
                                <label>Nama Kategori</label>
                                <input type="text" class="form-control" form="form-category" name="name">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="3" form="form-category" name="description"></textarea>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-add-category" onclick="addGroup()">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete">
                <p>Apakah Anda Yakin Ingin Menghapus Data Pasien Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
        var t = $('#table-category').DataTable( {
            "processing": true,
            // "serverSide": true,
            "ajax": "{{ url('/getAllContentGroups') }}",
            "columns": [
                {
                    "data": null,
                    "width": "20px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "name" },
                { "data": "description" },
                { "data": "created_at","width": "140px", "sClass": "text-center" },
                { "data": "updated_at","width": "150px", "sClass": "text-center" },
                // { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                //         if (row.atlet_count == 0) {
                //             return row.jml_atlet;
                //         } else {
                //             return row.atlet_count;
                //         }
                //     } 
                // },
                { "data": "null","width": "150px", render: function ( data, type, row ) {
                        return `
                            <a href="{{ url('contents') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteGroup(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                            `;
                    },"bSortable": false
                }
            ],
            
            // "fnCreatedRow": function (row, data, index) {
            //     $('td', row).eq(0).html(index + 1);
            // }
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function addGroup() {
        $('input').removeClass('is-invalid');
        $('.help-block').text('');

        $.ajax({    
            type: "post",
            url: "{{ url('contents') }}",
            data: $('#form-category').serialize(),
            dataType: "json",
            beforeSend: function(){
                $('#btn-add-category').text('Menyimpan Data ...');
                $('#btn-add-category').prop('disable', true);
            },
            success : function(data) {
                if (!data.error) {
                    location.reload();
                } else {
                    for (var key of Object.keys(data.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().text(data.error[key]);
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                }
            },
            complete:function(data) {
                $('#btn-add-category').text('Simpan');
                $('#btn-add-category').prop('disable', false);
            }
        });
    }

    function deleteGroup(id) {
        $('#input-delete').val(id);
        $('#modal-delete').modal('show');
        $('#btn-delete').click(function() {
            $.ajax({
                type: "get",
                url: "{{ url('contents') }}/"+$('#input-delete').val()+"/delete",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: "json",
                beforeSend:function () {
                    $('#btn-delete').text('Menghapus ...');
                    $('#btn-delete').prop('disable', true);
                },
                success: function (response) {
                    if (!response.error) {
                        location.reload();
                    } else {
                        alert(response.error);
                        $('#btn-delete').text('Hapus');
                        $('#btn-delete').prop('disable', false);
                    }
                }
            });
        });
    }

</script>

@endsection