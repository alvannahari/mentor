@extends('template/layout')

@section('title', 'Dashboard')

@section('content')
{{-- @if (session('msg'))
  <b class="text-success">{{ session('msg') }}</b>
@endif --}}
<div class="row">
  <div class="col-md-6 col-xs-12">
      <div class="card">
          <div class="card-body" style="align-items: center">
            <span class="card-title pt-3" style="border-bottom: none"> 
              <span style="font-size: 18px;">Aktivitas Pengguna </span>
            </span>
            <canvas class="mt-2" id="chart-user" style="width:100%" height="118">
          </div>
      </div>
  </div>
  <!-- column -->
  <div class="col-md-6 col-xs-12">
      <div class="card">
          <div class="card-body">
              <div class="row">
                  <div class="col-6">
                      <center>
                        <span class="card-title" style="border-bottom: none"> 
                          <span style="font-size: 18px;" >Jenis Kategori</span>
                        </span>
                      </center>
                      <canvas class="mt-2" id="chart-learning" width="250" height="200" style="display: inline-block;height: auto;width: inherit;"></canvas>
                  </div>
                  <div class="col-6" >
                      <center>
                        <span class="card-title" style="border-bottom: none"> 
                          <span style="font-size: 18px;" >Jumlah Konten</span>
                        </span>
                      </center>
                      <canvas class="mt-2" id="chart-kognitif" width="250" height="200" style="display: inline-block;height: auto;width: inherit;"></canvas>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="row">
  <!-- column -->
  <div class="col-12">
      <div class="card">
        <span class="card-title pt-3 px-3" style="border-bottom: none"> 
          <span style="font-size: 18px;" >Riwayat Aktivitas Pengguna </span>
        </span>
        <div class="container">
          <div class="container">
            <div class="table-responsive" style="max-height:450px;">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="border-top-0" style="width: 5px">NO.</th>
                            <th class="border-top-0">NAMA SISWA</th>
                            <th class="border-top-0">NAMA KONTEN</th>
                            <th class="border-top-0"><center>TIPE</center></th>
                            <th class="border-top-0" style="width: 170px"><center>TANGGAL WAKTU</center></th>
                        </tr>
                    </thead>
                    <tbody>
                      @forelse ($histories as $history)
                        <tr>
                          <td><span class="font-medium">{{ $loop->iteration }}</span></td>
                          <td class="txt-oflo">{{ $history['user']['first_name'].' '.$history['user']['last_name'] }}</td>
                          <td> {{ $history['content']['name'].' - '.$history['content']['content_group']['name'] }} </td>
                          <td><center><span class="font-medium"> {{ $history['content']['type'] }} </span></center></td>
                        <td class="txt-oflo"><center>{{ $history['created_at'] }}</center></td>
                        </tr>
                      @empty
                        <tr>
                          <td colspan="4"><center>Belum Ada Aktivitas Pengguna</center></td>
                        </tr>
                      @endforelse
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

<script>
  $(function() {
    var sum = function(a, b) { return a + b; };

    var labelActivity = JSON.parse('{!! json_encode($activity['label']) !!}');
    var dataActivity = JSON.parse('{!! json_encode($activity['value']) !!}');

    var ctxUser = $("#chart-user")[0].getContext("2d");
    var chartUser = new Chart(ctxUser, {
        type: 'line',
        data: {
            datasets: [{
                data: dataActivity,
                backgroundColor: 'rgba(41, 182, 246,0.2)',
                borderColor: 'rgba(41, 182, 246, 1)',
                hoverBackgroundColor : 'white',
                borderWidth: 2,
                label : 'Jumlah Siswa Aktif'
            }],
            labels: labelActivity
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: true,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var ctxLearning = $("#chart-learning")[0].getContext("2d");
    var chartLearning = new Chart(ctxLearning, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [43,39,12,22,65],
                backgroundColor: ['#66BB6A', '#4FC3F7', '#F44336', '#FFEE58',],
                borderWidth: 1
            }],
            labels: ['Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5']
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: false,
            // maintainAspectRatio: true,
            cutoutPercentage: 1,
            plugins: {
                datalabels: {
                    color: 'black',
                    formatter: function(value, context) {
                        return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                    }
                }
            },
            legend: {
                display: true, 
                position:'bottom',
                labels: {
                    padding : 12,
                    fontColor: 'black',
                    fontSize : 11,
                    boxWidth: 20,
                    boxHeight: 2,
                },
            }
        }
    });

    var ctxKognitif = $("#chart-kognitif")[0].getContext("2d");
        var chartKognitif = new Chart(ctxKognitif, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [3,7,9,4,2],
                    backgroundColor: ['#66BB6A', '#4FC3F7', '#F44336', '#FFEE58',],
                    borderWidth: 1
                }],
                labels: ['Konten 1', 'Konten 2', 'Konten 3', 'Konten 4', 'Konten 5']
            },
            options: {
                plugins: {
                    datalabels: {
                        color: 'black',
                        formatter: function(value, context) {
                            return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                        }
                    }
                },
                responsive: false,
                cutoutPercentage: 1,
                legend: {
                    display: true, 
                    position:'bottom',
                    labels: {
                        padding : 12,
                        fontColor: 'black',
                        fontSize : 11,
                        boxWidth: 20,
                        boxHeight: 2,
                    },
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            },
        });
  });
</script>
@endsection