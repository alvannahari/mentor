@extends('template/layout')

@section('title', 'Data User')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh pengguna yang sudah melakukan proses registrasi. Data user termasuk detail user, riwayat dan hak akses untuk konten yang tersedia </h6>
                    </div>
                    {{-- <div class="col-md-2">
                        <button class="btn btn-primary m-b-10" data-toggle="modal" data-target="#modal-add-user" style="float: right;"><i class="mdi mdi-plus"></i> Tambah User</button>
                    </div> --}}
                </div>
                {{-- <h6 class="card-title m-t-40"><i class="m-r-5 font-18 mdi mdi-numeric-1-box-multiple-outline"></i> Table With Outside Padding</h6>  --}}
                <div class="table-responsive">
                    <table id="table-users" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>NAMA</th>
                                <th>EMAIL</th>
                                <th>GENDER</th>
                                <th>NO. TELEPON</th>
                                <th style="text-align: center">TERDAFTAR</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td style="width: 5px;text-align: center">2</td>
                                <td>Kategori Kedua</td>
                                <td>Deskripsi untuk Kategori Kedua</td>
                                <td style="text-align: center;width: 170px">21-04-2020 07:15:43</td>
                                <td style="text-align: center;width: 170px">25-04-2020 14:01:21</td>
                                <td style="width: 165px">
                                    <a href="#" class="btn btn-success btn-sm" data-placement="top" data-original-title="Detail"><i class="mdi mdi-magnify"></i> Detail</a>
                                    <button class="btn btn-sm btn-danger m-l-5" onclick="deleteGroup()" ><i class="mdi mdi-delete"></i> Delete</button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Add User --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah User Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-t-10" id="form-user" >
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                @csrf
                                <label>Nama Depan</label>
                                <input type="text" class="form-control" form="form-user" name="first_name">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Nama Belakang</label>
                                <input type="text" class="form-control" form="form-user" name="last_name">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" form="form-user" name="email">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <div class="row">
                                    <div class="col-6" style="text-align: center;">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio1" form="form-user" name="gender" class="custom-control-input" value="Laki-laki" checked>
                                            <label class="custom-control-label" for="customRadio1">Laki-laki</label>
                                        </div>
                                    </div>
                                    <div class="col-6" style="text-align: center;">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio2" form="form-user" name="gender" class="custom-control-input" value="Perempuan">
                                            <label class="custom-control-label" for="customRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>No. Telepon</label>
                                <input type="text" class="form-control" form="form-user" name="phone_number">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-add-user" onclick="addUser()">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete">
                <p>Apakah Anda Yakin Ingin Menghapus Data Pasien Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
        var t = $('#table-users').DataTable( {
            "processing": true,
            // "serverSide": true,
            "ajax": "{{ url('getAllUsers') }}",
            "columns": [
                {
                    "data": null,
                    "width": "20px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", render: function ( data, type, row ) {
                        return row.first_name+' '+row.last_name;
                    }
                },
                { "data": "email" },
                { "data": "gender"},
                { "data": "phone_number"},
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", render: function ( data, type, row ) {
                        return `
                            <a href="{{ url('user') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteUser(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                            `;
                    },"bSortable": false
                }
            ],
            
            // "fnCreatedRow": function (row, data, index) {
            //     $('td', row).eq(0).html(index + 1);
            // }
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function addUser() {
        $('input').removeClass('is-invalid');
        $('.help-block').text('');

        $.ajax({    
            type: "post",
            url: "{{ url('user') }}",
            data: $('#form-user').serialize(),
            dataType: "json",
            beforeSend: function(){
                $('#btn-add-user').text('Menyimpan Data ...');
                $('#btn-add-user').prop('disable', true);
            },
            success : function(data) {
                // console.log(data.error);
                if (!data.error) {
                    location.reload();
                    // console.log(data.data);
                } else {
                    for (var key of Object.keys(data.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().text(data.error[key]);
                        // console.log(key + " -> " + data.error[key])
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                }
            },
            complete:function(data) {
                $('#btn-add-user').text('Simpan');
                $('#btn-add-user').prop('disable', false);
            }
        });
    }

    function deleteUser(id) {
        $('#input-delete').val(id);
        $('#modal-delete').modal('show');
        $('#btn-delete').click(function() {
            $.ajax({
                type: "get",
                url: "{{ url('user') }}/"+$('#input-delete').val()+"/delete",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: "json",
                beforeSend:function () {
                    $('#btn-delete').text('Menghapus ...');
                    $('#btn-delete').prop('disable', true);
                },
                success: function () {
                    location.reload();
                },
                complete:function () {
                    $('#btn-delete').text('Hapus');
                    $('#btn-delete').prop('disable', false);
                }
            });
        });
    }

</script>

@endsection