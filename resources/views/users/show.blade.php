@extends('template.layout')

@section('title')
    Detail User
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="card">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Info User </span>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-sm btn-danger" style="float: right" onclick="remove(0, {{ $user['id'] }})"> <i class="mdi mdi-delete"></i> Delete</button>
                        <button class="btn btn-sm btn-warning m-r-15" style="float: right" data-toggle="modal" data-target="#modal-update-user"> <i class="mdi mdi-update"></i> Ubah</button>
                    </div>
                </div>
            </span>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label>Nama Lengkap</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['first_name'].' '.$user['last_name'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Email</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['email'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Gender</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['gender'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >No. Telepon</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['phone_number'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Tanggal Dibuat</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['created_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4" style="text-align: right">
                            <label >Terakhir Diperbarui</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['updated_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="card" style="height: 369px;">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Akses Konten </span>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-sm btn-warning" style="float: right" data-toggle="modal" data-target="#modal-add-content"> <i class="mdi mdi-update"></i> Ubah</button>
                    </div>
                </div>
            </span>
            <div class="card-body pt-0">
                <div style="width:100%;display: block;height: 200px;overflow-y: scroll;">
                <table class="table table-hover table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr style="line-height: 2.2">
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA</th>
                            <th style="border-top: none;">TANGGAL</th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['content_groups'] as $content)
                            <tr>
                                <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                <td>{{ $content['name'] }}</td>
                                <td>{{ $content['created_at'] }}</td>
                                <td style="width: 80px">
                                    <a href="{{ url('/contents').'/'.$content['pivot']['content_group_id'] }}" class="btn btn-success btn-sm" data-placement="top" data-original-title="Lihat"><i class="mdi mdi-magnify"></i> Lihat</a>
                                    {{-- <button class="btn btn-sm btn-danger m-l-5" onclick="removeUser({{ $user['id'] }})" ><i class="mdi mdi-delete"></i> Delete</button> --}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak Ada Akses Konten</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="card">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Riwayat Komentar </span>
                    </div>
                </div>
            </span>
            <div class="card-body pt-0 px-1" >
                <div class="container">
                <table class="comment table table-hover table-sm" width="100%" >
                    <tbody>
                        @forelse ($user['comment'] as $comment)
                            <tr>
                                <td class="px-2">{{ $loop->iteration }}</td>
                                <td class="px-2" style="text-align: left;">
                                    <p style="margin-bottom: 0.4rem"><b>{{ $comment['content']['name'] }}</b></p>
                                    <p style="margin-bottom: 0.6rem;">
                                        {{ $comment['comment'] }}
                                    </p>
                                </td>
                                <td class="px-2" >{{ $comment['created_at'] }}</td>
                                <td class="px-2" style="width: 80px">
                                    <button class="btn btn-sm btn-danger m-l-5" onclick="remove(1, {{ $comment['id'] }})"><i class="mdi mdi-delete"></i> Delete</button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Pengguna Belum Pernah Memberikan Komentar !</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="card">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Riwayat Pengguna </span>
                    </div>
                </div>
            </span>
            <div class="card-body pt-0" >
                <table class=" table table-hover table-sm" width="100%" >
                    <thead>
                        <tr>
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA KONTEN</th>
                            <th style="border-top: none;">TIPE</th>
                            <th style="border-top: none;"><center>TANGGAL AKSES</center></th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['history'] as $history)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $history['content']['name'] }}</td>
                                <td>{{ $history['content']['type'] }}</td>
                                <td><center>{{ $history['created_at'] }}</center></td>
                                <td style="width: 80px">
                                    <a href="{{ url('content').'/'.$history['content_id'] }}" class="btn btn-sm btn-success" title="Detail"><i class="mdi mdi-magnify"></i> Lihat</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak Ada Aktivitas Pengguna</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- Modal Update User --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-user">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Data User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-t-10" id="form-user" >
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                @csrf
                                <label>Nama Depan</label>
                                <input type="text" class="form-control" form="form-user" name="first_name" value="{{ $user['first_name'] }}">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Nama Belakang</label>
                                <input type="text" class="form-control" form="form-user" name="last_name" value="{{ $user['last_name'] }}">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" form="form-user" name="email" value="{{ $user['email'] }}">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <div class="row">
                                    <div class="col-6" style="text-align: center;">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio1" name="gender" form="form-user" class="custom-control-input" value="Laki-laki" {{ $user['gender'] == 'Laki-laki' ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="customRadio1">Laki-laki</label>
                                        </div>
                                    </div>
                                    <div class="col-6" style="text-align: center;">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio2" name="gender" form="form-user" class="custom-control-input" value="Perempuan" {{ $user['gender'] == 'Perempuan' ? 'checked' : '' }} >
                                            <label class="custom-control-label" for="customRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>No. Telepon</label>
                                <input type="text" class="form-control" form="form-user" name="phone_number" value="{{ $user['phone_number'] }}">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-submit-update-user">Simpan</button>
            </div>
        </div>
    </div>
</div>

{{-- Modal Add Access Content --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-content">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Tambah Akses Konten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form class=" m-t-10" id="form-access">
                @csrf
                <table id="table-users" class="table table-hover table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr style="line-height: 2.2">
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA KATEGORI</th>
                            <th style="border-top: none;">DESCRIPTION</th>
                            <th style="border-top: none;">AKSES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($contents as $content)
                            <tr>
                                <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                <td>{{ $content['name'] }}</td>
                                <td>{{ $content['description'] }}</td>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <?php $checked = in_array($content['id'], $roles) ? true : false; ?>
                                        {{ Form::checkbox('content[]', $content['id'], $checked, ['class'=>'custom-control-input', 'form' => 'form-access' ,'id' => 'customCheck'.$loop->iteration]) }}
                                        <label class="custom-control-label" for="customCheck{{ $loop->iteration }}">Diberikan</label>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Pengguna Belum Mempunyai Akses</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-info" id="btn-update-access">Simpan</button>
        </div>
        </div>
    </div>
</div>

{{-- Modal Delete --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Konfirmasi Hapus</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="input-delete">
            <p>Apakah Anda Yakin Ingin Menghapus Data Ini ? </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
        </div>
        </div>
    </div>
</div>

<script>

    $('#btn-submit-update-user').click(function (){
        var button = $(this);
        emptyFormInput();
        $.ajax({    
            type: "patch",
            url: "{{ url('user').'/'.$user['id'] }}",
            data: $('#form-user').serialize(),
            dataType: "json",
            beforeSend: function(){
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            success : function(response) {
                // console.log(response.error);
                if (!response.error) {
                    location.reload();
                    // console.log(response.response);
                } else {
                    for (var key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                        // console.log(key + " -> " + response.error[key])
                    }
                    alert('Data User Gagal Diperbarui!. Terdapat Kesalahan.');
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    });

    function remove(temp, id) {
        $('#input-delete').val(id);
        $('#modal-delete').modal('show');
        if (temp == 0) {
            var url = "{{ url('user') }}/"+$('#input-delete').val()+"/delete";
        } else {
            var url = "{{ url('comment') }}/"+$('#input-delete').val()+"/delete";
        }
        $('#btn-delete').click(function() {
            var button = $(this);
            $.ajax({
                type: "get",
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: "json",
                beforeSend:function () {
                    button.html('Menghapus ...');
                    button.prop('disable', true);
                },
                success: function (response) {
                    if (!response.error) {
                        if (temp == 0) {
                            // window.location = "/user";
                            history.back();
                        } else {
                            location.reload();
                        }
                    } else {
                        alert(response.error);
                        button.html('Hapus');
                        button.prop('disable', false);
                    }
                }
            });
        });
    };

    $('#btn-update-access').click( function() {
        var button = $(this);
        button.html('Menyimpan...');
        button.prop('disable', true);
        $.ajax({
            type: "post",
            url: "{{ url('accessUsers').'/'.$user['id'] }}",
            data: $('#form-access').serialize(),
            success: function (response) {
                if (!response.error) {
                    location.reload();
                }
                // console.log(response.data);
            }
        });
    });

</script>
@endsection