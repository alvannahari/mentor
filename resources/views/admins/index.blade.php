@extends('template/layout')

@section('title', 'Data Admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh admin yang telah didaftarkan oleh pihak pengembang. Data admin termasuk hak akses untuk melakukan pengolahan data pada website admin</h6>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary m-b-10" data-toggle="modal" data-target="#modal-add-admin" style="float: right;"><i class="mdi mdi-plus"></i> Tambah Admin</button>
                    </div>
                </div>
                {{-- <h6 class="card-title m-t-40"><i class="m-r-5 font-18 mdi mdi-numeric-1-box-multiple-outline"></i> Table With Outside Padding</h6>  --}}
                <div class="table-responsive">
                    <table id="table-admins" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>USERNAME</th>
                                <th>NAMA LENGKAP</th>
                                <th style="text-align: center">TERDAFTAR</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td style="width: 5px;text-align: center">2</td>
                                <td>Kategori Kedua</td>
                                <td>Deskripsi untuk Kategori Kedua</td>
                                <td style="text-align: center;width: 170px">21-04-2020 07:15:43</td>
                                <td style="text-align: center;width: 170px">25-04-2020 14:01:21</td>
                                <td style="width: 165px">
                                    <a href="#" class="btn btn-success btn-sm" data-placement="top" data-original-title="Detail"><i class="mdi mdi-magnify"></i> Detail</a>
                                    <button class="btn btn-sm btn-danger m-l-5" onclick="deleteGroup()" ><i class="mdi mdi-delete"></i> Delete</button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Add Admin --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-admin">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-t-10" id="form-admin" >
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                @csrf
                                <label>Nama Depan</label>
                                <input type="text" class="form-control" form="form-admin" name="first_name">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Nama Belakang</label>
                                <input type="text" class="form-control" form="form-admin" name="last_name">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" form="form-admin" name="username">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" form="form-admin" name="password">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password</label>
                                <input type="password" class="form-control" form="form-admin" name="c_password">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-add-category" onclick="addAdmin()">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete">
                <p>Apakah Anda Yakin Ingin Menghapus Data Pasien Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
        var t = $('#table-admins').DataTable( {
            "processing": true,
            // "serverSide": true,
            "ajax": "{{ url('getAllAdmins') }}",
            "columns": [
                {
                    "data": null,
                    "width": "20px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "username" },
                { "data": "null", render: function ( data, type, row ) {
                        return row.first_name+' '+row.last_name;
                    }
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", render: function ( data, type, row ) {
                        return `
                            <button class="btn btn-sm btn-warning m-l-5" onclick="updateAdmin(`+row.id+`)"><i class="mdi mdi-update"></i> Ubah</button>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteAdmin(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                            `;
                    },"bSortable": false
                }
            ],
            
            // "fnCreatedRow": function (row, data, index) {
            //     $('td', row).eq(0).html(index + 1);
            // }
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function addAdmin() {
        $('input').removeClass('is-invalid');
        $('.help-block').text('');

        $.ajax({    
            type: "post",
            url: "{{ url('contents') }}",
            data: $('#form-admin').serialize(),
            dataType: "json",
            beforeSend: function(){
                $('#btn-add-admin').text('Menyimpan Data ...');
                $('#btn-add-admin').prop('disable', true);
            },
            success : function(data) {
                // console.log(data.error);
                if (!data.error) {
                    location.reload();
                    // console.log(data.data);
                } else {
                    for (var key of Object.keys(data.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().text(data.error[key]);
                        // console.log(key + " -> " + data.error[key])
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                }
            },
            complete:function(data) {
                $('#btn-add-admin').text('Simpan');
                $('#btn-add-admin').prop('disable', false);
            }
        });
    }

    function updateAdmin(id) {
        alert('Fitur Masih Dalam Tahap Perkembangan !');
    }

    function deleteAdmin(id) {
        $('#input-delete').val(id);
        $('#modal-delete').modal('show');
        // $('#btn-delete').click(function() {
        //     $.ajax({
        //         type: "delete",
        //         url: "{{ url('user') }}/"+$('#input-delete').val(),
        //         data: {
        //             "_token": "{{ csrf_token() }}",
        //         },
        //         dataType: "json",
        //         beforeSend:function () {
        //             $('#btn-delete').text('Menghapus ...');
        //             $('#btn-delete').prop('disable', true);
        //         },
        //         success: function () {
        //             location.reload();
        //         },
        //         complete:function () {
        //             $('#btn-delete').text('Hapus');
        //             $('#btn-delete').prop('disable', false);
        //         }
        //     });
        // });
    }

</script>

@endsection