<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Superuser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {
    
    function index() {
        return view('login');
    }

    function login(Request $request) {
        $credentials = $request->only(Admin::USERNAME, Admin::PASSWORD);
        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->route('home');
        } else if (Auth::guard('super')->attempt($credentials)) {
            return redirect()->route('home');
        }

        return redirect()->route('login')->with('');
    }

    function logout() {
        foreach(array_keys(config('auth.guards')) as $guard){
            if(auth()->guard($guard)->check()) {
                Auth::guard($guard)->logout();
                return redirect()->route('login');
            };
        }
    }
}
