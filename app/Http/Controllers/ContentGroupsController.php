<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContentGroup;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserContentGroups;

class ContentGroupsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('contents.index');
    }

    function getAllContentGroups() {
        $groupContents = ContentGroup::get()->toArray();
        return response(['data' => $groupContents]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = $this->_validateContents($request);

        if ($validator->fails()) { 
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $this->_credentials($request);
        $credentials[ContentGroup::ID_SUPER] = 1;

        ContentGroup::create($credentials);

        return response(['error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ContentGroup $contents) {
        $content = ContentGroup::with(['content', 'users'])->find($contents->id)->toArray();
        $users = User::get()->toArray();
        // $roles = ContentGroup::with(['users'])->find($$contents->id)->users->toArray();
        $roles = ContentGroup::find($contents->id)->users()->pluck('users.id')->toArray();
        // sort($roles);
        // dd($roles);
        // dd($content);
        return view('contents.show', compact('content','users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentGroup $contentGroup) {
        $validator = $this->_validateContents($request);

        if ($validator->fails()) { 
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $this->_credentials($request);

        if ($contentGroup->update($credentials)) {
            return response(['error' => false]);
        }
        return response(['error' => 'Data Kategori Konten Gagal Diperbarui !']);
    }

    function updateAccessContents(Request $request, ContentGroup $contentGroup) {
        $credentials = array_values($request->only('user'))[0];
        // $constraint = array_values($credentials)[0];
        if($contentGroup->users()->sync($credentials)) {
            return response(['error' => false]);
        };
        return response(['error' => 'Data Akses Konten Gagal Diperbarui']);
        // return response(['data' => $constraint[0]]);
        // return response(['data' => $contentGroup->users()->allRelatedIds()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentGroup $contentGroup) {
        // ContentGroup::find($contentGroup->id)->delete();
        if($contentGroup->delete()) {
            return response(['error' => false]);
        };
        return response(['error' => 'Data Kategori Konten Tidak Bisa Dihapus']);
    }

    private function _validateContents(Request $request) {
        $validator = Validator::make($request->all(), [
            ContentGroup::NAME          => 'required',
            ContentGroup::DESCRIPTION   => 'required'
        ]);

        return $validator;
    }

    private function _credentials(Request $request) {
        $credentials = $request->only([ContentGroup::NAME, ContentGroup::DESCRIPTION]);

        return $credentials;
    }
}
