<?php

namespace App\Http\Controllers;

use App\Models\ContentGroup;
use App\Models\User;
use App\Models\UserContentComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('users.index');
    }

    public function getAllUsers() {
        $users = User::get()->toArray();
        return response(['data' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = $this->_validate($request);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        };

        $credentials = $this->_credentials($request);
        $credentials[User::PASSWORD] = bcrypt('testuser');
        User::create($credentials);

        return response(['error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $users) {
        $user = User::with(['contentGroups', 'comment.content', 'history.content'])->find($users->id)->toArray();
        $contents = ContentGroup::get()->toArray();
        $roles = User::find($users->id)->contentGroups()->pluck('content_groups.id')->toArray();
        // dd($user);

        return view('users.show', compact('user', 'contents', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        $validator = $this->_validate($request);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        };

        $credentials = $this->_credentials($request);

        if($user->update($credentials)) {
            return response(['error' => false]);
        };

        return response(['error' => true,]);
    }

    function updateAccessUsers(Request $request, User $user) {
        $credentials = array_values($request->only('content'))[0];
        // $constraint = array_values($credentials)[0];
        $user->contentGroups()->sync($credentials);
        // return response(['data' => $constraint[0]]);
        // return response(['data' => $contentGroup->users()->allRelatedIds()]);
        return response(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        if ($user->delete()) {
            return response(['error' => false]);
        };
        return response(['error' => 'Gagal Menghapus User. Terdapat Kesalahan!']);
    }

    private function _validate(Request $request) {
        $validator = Validator::make($request->all(), [
            User::FIRST_NAME    => 'required',
            User::EMAIL         => 'required',
            User::PHONE         => 'required'
        ]);
        return $validator;
    }

    private function _credentials(Request $request) {
        $credentials = $request->only(User::FIRST_NAME, User::EMAIL, User::GENDER, USER::LAST_NAME, User::PHONE);
        return $credentials;
    }
}
