<?php

namespace App\Http\Controllers;

use App\Models\UserContentHistories;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {
    
    function index() {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $getActivity = UserContentHistories::whereBetween(UserContentHistories::CREATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->groupBy('date')->select(DB::raw('DATE_FORMAT(created_at, "%d") as date'), DB::raw('count(*) as total'))->get()->toArray();
        $activity = $this->_graphActivity($getActivity);

        $histories = UserContentHistories::with('user', 'content.contentGroup')->get()->toArray();
        // $getActivity = UserContentHistories::groupBy('date')->orderBy('date', 'desc')->get(array(
        //     DB::raw('Date(created_at) as date'),
        //     DB::raw('count(*) as total')
        // ));
        // dd($getActivity->toArray());
        
        // dd($activity);
        return view('home', compact('histories', 'activity'));
    }

    private function _graphActivity(array $activity) {
        $data['value'] = [];

        $data['label'] = [
            'Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'
        ];

        // foreach($nameDay as $index => $name) {
        for ($i=0; $i<7; $i++){
            $temp = false;
            $thisDate = Carbon::now()->startOfWeek()->addDay($i)->format('d');
            foreach($activity as $key => $value) {
                if($value['date'] == $thisDate){
                    $data['value'][$i] = $value['total'];
                    $temp = true;
                    break;
                };
            };
            if(!$temp) {
                $data['value'][$i] = 0;
            }
            // if(in_array($thisDate, array_column($data, 'date'))) {
            //     $dateWeek[$thisDate] = $i;
            // } else {
            //     $dateWeek[$thisDate] = 0;
            // }
        };

        return $data;
    }
}
