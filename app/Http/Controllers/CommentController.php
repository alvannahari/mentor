<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;
use App\Models\UserContentComment;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Content $content) {
        $validator = $this->_validate($request);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        };

        $credentials = $this->_credentials($request);
        $credentials[UserContentComment::ID_CONTENT] = $content->id;
        // $credentials[UserContentComment::ID_USER] = auth()->user()->id;
        $credentials[UserContentComment::ID_USER] = 0;

        UserContentComment::create($credentials);

        return response(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserContentComment $userContentComment) {
        if ($userContentComment->delete()) {
            return response(['error' => false]);
        };
        return response(['error' => 'Gagal Menghapus Komentar. Terdapat Kesalahan!']);
    }

    private function _validate(Request $request) {
        $validator = Validator::make($request->all(), [
            UserContentComment::COMMENT     => 'required'
        ]);

        return $validator;
    }

    private function _credentials(Request $request) {
        $credentials = $request->only(UserContentComment::COMMENT);

        return $credentials;
    }
}
