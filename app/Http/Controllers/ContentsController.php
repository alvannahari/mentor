<?php

namespace App\Http\Controllers;

use App\Models\ContentGroup;
use Illuminate\Http\Request;
use App\Models\Content;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class ContentsController extends Controller {

    const URL_PATH = 'assets/file_content';

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ContentGroup $contentGroup) {
        $validator = $this->_validate($request);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        };

        $now = Carbon::now();
        $credentials = $this->_credentials($request);
        $credentials[Content::ID_GROUP] = $contentGroup->id;

        if ($request->type == 'Pdf') {
            $credentials[Content::URL] = 1;
            $create = Content::create($credentials);
            $uid = $create->id;
            if ($files = $request->file(Content::URL)) { 
                $destinationPath = public_path(SELF::URL_PATH);
                $filename = $uid.'_konten_'.$credentials[Content::NAME].'_'.$now->format('Y-m-d_H-i-s').'.'.$files->getClientOriginalExtension();
                $files->move($destinationPath, $filename);
            };
            Content::where('id', $uid)->update([
                Content::URL        => $filename
            ]);
        } else {
            $create = Content::create($credentials);
        }

        return response(['error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content) {
        $content = Content::with(['comment.user', 'history.user'])->find($content->id)->toArray();
        // dd($content);

        return view('contents.showContent', compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content) {
        $validator = Validator::make($request->all(), [
            Content::NAME           => 'required',
            Content::DESCRIPTION    => 'required'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        };

        $credentials = $request->only(Content::NAME, Content::DESCRIPTION);
        if ($content->update($credentials)) {
            return response(['error' => false]);
        }
        return response(['error' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content) {
        $filepath = public_path().'/'.SELF::URL_PATH.'/'.$content->url;
        File::delete($filepath);
        if ($content->delete()) {
            Session::flash('success', 'Data Konten Berhasil Dihapus!');
            return response(['error' => false]);
        }
        Session::flash('failed', 'Data Konten Gagal Dihapus!');
        return response(['error' => 'Konten Gagal Terhapus']);
    }

    private function _validate(Request $request) {
        if ($request->type == 'Pdf') {
            $validator = Validator::make($request->all(), [
                Content::NAME           => 'required',
                Content::DESCRIPTION    => 'required',
                Content::URL            => 'required|mimes:doc,docx,pdf|max:8224',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                Content::NAME           => 'required',
                Content::DESCRIPTION    => 'required',
                Content::URL            => 'required'
            ]);
        }
        return $validator;
    }

    private function _credentials(Request $request) {
        if ($request->type == 'Pdf') {
            $credentials = $request->only([Content::NAME, Content::DESCRIPTION, Content::TYPE]);
        } else {
            $credentials = $request->only([Content::NAME, Content::DESCRIPTION, Content::TYPE, Content::URL]);
        }
        $credentials[Content::ID_SUPER] = 1;

        return $credentials;
    }
}
