<?php

namespace App\Http\Controllers\Api\v1\Contents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserContentGroups;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class getUserAuthorizedGroups extends Controller {
    
    // End Point Error
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            UserContentGroups::ID_USER
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $groupContent = UserContentGroups::groupBy(UserContentGroups::ID_GROUP)->select(UserContentGroups::ID_GROUP, DB::raw('count(*) as total'))->get();
        // $content = UserContentGroups::where('user_id',$request[UserContentGroups::ID_USER])->get('content_group_id');
        $dataUserContent = User::with('contentGroups.content')->find($request[UserContentGroups::ID_USER]);
        
        foreach ($dataUserContent['contentGroups'] as $key => $val) {
            foreach ($groupContent as $keyGroup => $valGroup) {
                if ($val[UserContentGroups::ID] == $valGroup[UserContentGroups::ID_GROUP]) {
                    $val['total_user'] = $valGroup['total'];
                }
            }

            $totalContent = 0;
            $totalVideo = 0;
            $totalPdf = 0;

            foreach ($val['content'] as $keyContent => $valContent) {
                if ($valContent['type'] == 'Pdf') {
                    $totalPdf++;
                } else if ($valContent['type'] == 'Video') {
                    $totalVideo++;
                };
                $totalContent++;
                // $val['total_content'] = 4;
            }
            
            $val['total_content'] = $totalContent;
            $val['total_content_pdf'] = $totalPdf;
            $val['total_content_video'] = $totalVideo;
            unset($val['content']);
        }

        return APIresponse(true, 'Data User Konten Berhasil Didapatkan!', $dataUserContent);
    }
}
