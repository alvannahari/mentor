<?php

namespace App\Http\Controllers\Api\v1\Contents;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserContentGroups;
use App\Models\User;
use App\Models\UserContentComment;
use Illuminate\Support\Facades\DB;

class getUserAuthorizedContents extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            UserContentGroups::ID_USER
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        $user_id = $request[UserContentGroups::ID_USER];

        $countGroupContent = UserContentGroups::groupBy(UserContentGroups::ID_GROUP)->select(UserContentGroups::ID_GROUP, DB::raw('count(*) as total'))->get();
        $countComment = UserContentComment::groupBy(UserContentComment::ID_CONTENT)->select(UserContentComment::ID_CONTENT, DB::raw('count(*) as total'))->get();
        $dataUserContent = User::with([
            'contentGroups.content.comment' => function($q) use ($user_id) {
                $q->where('user_id', $user_id)->orderBy('created_at', 'desc')->take(2);
            }, 
            'contentGroups.content.history' => function($q) use ($user_id) {
                $q->where('user_id', $user_id)->orderBy('created_at', 'desc')->take(2);
            }
        ])->find($user_id);
        
        if ($dataUserContent) {
            foreach ($dataUserContent['contentGroups'] as $key => $val) {
                foreach ($countGroupContent as $keyGroup => $valGroup) {
                    if ($val[UserContentGroups::ID] == $valGroup[UserContentGroups::ID_GROUP]) {
                        $val['total_user'] = $valGroup['total'];
                    }
                }
    
                $totalContent = 0;
                $totalVideo = 0;
                $totalPdf = 0;
    
                foreach ($val['content'] as $keyContent => $valContent) {
                    if ($valContent['type'] == 'Pdf') {
                        $valContent['url'] = url('/').API_FILE_PATH.$valContent['url'];
                        $totalPdf++;
                    } else if ($valContent['type'] == 'Video') {
                        $totalVideo++;
                    };
                    $totalContent++;

                    if (sizeof($valContent['history']) > 0) {
                        $valContent['is_viewed'] = true;
                        $valContent['last_viewed_date'] = $valContent['history'][0]['created_at']->format('Y-m-d H:i:s');
                    } else {
                        $valContent['is_viewed'] = false;
                        $valContent['last_viewed_date'] = null;
                    }

                    if (sizeof($valContent['comment']) > 0) {
                        $valContent['is_commented'] = true;
                        $valContent['last_commented_date'] = $valContent['comment'][0]['created_at']->format('Y-m-d H:i:s');
                    } else {
                        $valContent['is_commented'] = false;
                        $valContent['last_commented_date'] = null;
                    }

                    foreach ($countComment as $keyComment => $valComment) {
                        if ($valComment['content_id'] == $valContent[Content::ID]) {
                            $valContent['total_comment'] = $valComment['total'];
                            break;
                        } else {
                            $valContent['total_comment'] = 0;
                        }
                    }

                    unset($valContent['history']);
                    unset($valContent['comment']);
                }
                
                $val['total_content'] = $totalContent;
                $val['total_content_pdf'] = $totalPdf;
                $val['total_content_video'] = $totalVideo;
            }
        }

        return APIresponse(true, 'Data User Konten Berhasil Didapatkan!', $dataUserContent);
    }
}
