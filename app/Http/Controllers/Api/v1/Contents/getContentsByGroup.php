<?php

namespace App\Http\Controllers\Api\v1\Contents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ContentGroup;
use App\Models\Content;

class getContentsByGroup extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            ContentGroup::ID   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };
        
        $contentGroup = ContentGroup::with('content.comment')->find($request->id);

        foreach ($contentGroup['content'] as $keyContent => $valContent) {
            $total_comment = 0;
            if ($valContent[Content::TYPE] == 'Pdf') {
                $valContent[Content::URL] = url('/').API_FILE_PATH.$valContent[Content::URL];
            }
            foreach ($valContent['comment'] as $keyComment => $valComment) {
                $total_comment++;
            }
            $valContent['total_comment'] = $total_comment;
            unset($valContent['comment']);
        }

        return APIresponse(true, 'Data Kategori Konten Berhasil Ditemukan!', $contentGroup);
    }
}
