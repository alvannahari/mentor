<?php

namespace App\Http\Controllers\Api\v1\Contents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserContentComment;

class submitComment extends Controller {
    
    function __invoke(Request $request) {
        $request = $request->toArray();

        $validator = Validator::make($request, [
            UserContentComment::ID_USER     => 'required',
            UserContentComment::ID_CONTENT  => 'required',
            UserContentComment::COMMENT     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        UserContentComment::create($request);

        return APIresponse(true, 'Komentar Baru Berhasil Disimpan!', null);
    }
}
