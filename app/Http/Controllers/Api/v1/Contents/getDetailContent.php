<?php

namespace App\Http\Controllers\Api\v1\Contents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Content;

class getDetailContent extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Content::ID     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $data = Content::with('comment')->find($request->id);

        if ($data[Content::TYPE] == 'Pdf') {
            $data[Content::URL] = url('/').API_FILE_PATH.$data[Content::URL];
        }

        return APIresponse(true, 'Data Konten Berhasil Didapatkan!', $data);
    }
}
