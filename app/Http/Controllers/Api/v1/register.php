<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class register extends Controller{
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::EMAIL         => 'required|email',
            User::PASSWORD      => 'required',
            User::PHONE         => 'required|numeric',
            User::FIRST_NAME    => 'required',
            User::GENDER        => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        $request[User::PASSWORD] = bcrypt($request[User::PASSWORD]);
        $data = User::create($request);
        $data['token'] =  $data->createToken('register')-> accessToken; 

        return APIresponse(true, 'Registrasi Akun Berhasil!', $data);
    }
}
