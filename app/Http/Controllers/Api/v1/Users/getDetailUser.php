<?php

namespace App\Http\Controllers\Api\v1\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class getDetailUser extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::ID    => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $data = User::find($request->id);

        return APIresponse(true, 'Data User Berhasil Didapatkan!', $data);
    }
}
