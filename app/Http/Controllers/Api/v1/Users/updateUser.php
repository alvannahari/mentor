<?php

namespace App\Http\Controllers\Api\v1\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class updateUser extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::ID            => 'required',
            User::EMAIL         => 'required|email',
            User::PASSWORD      => 'required',
            User::PHONE         => 'required|numeric',
            User::LAST_NAME     => 'required',
            User::GENDER        => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        User::where(User::ID, $request[User::ID])->update([
            User::EMAIL         => $request[User::EMAIL],
            User::PASSWORD      => bcrypt($request[User::PASSWORD]),
            User::PHONE         => $request[User::PHONE],
            User::FIRST_NAME    => $request[User::FIRST_NAME],
            User::LAST_NAME     => $request[User::LAST_NAME] ?? null,
            User::GENDER        => $request[User::GENDER]
        ]);

        return APIresponse(true, 'Data User Berhasil Diperbarui!', null);

    }
}
