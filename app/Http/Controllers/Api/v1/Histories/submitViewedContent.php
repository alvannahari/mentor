<?php

namespace App\Http\Controllers\Api\v1\Histories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserContentHistories;

class submitViewedContent extends Controller {
    
    function __invoke(Request $request) {
        $request = $request->toArray();

        $validator = Validator::make($request, [
            UserContentHistories::ID_USER       => 'required',
            UserContentHistories::ID_CONTENT    => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        UserContentHistories::updateOrCreate([
            UserContentHistories::ID_USER     => $request[UserContentHistories::ID_USER],
            UserContentHistories::CREATED_AT  => UserContentHistories::where('created_at', 'LIKE', date('Y-m-d').' %')->first()->created_at ?? date('Y-m-d H:i:s')
        ],$request);

        return APIresponse(true, 'History Konten Berhasil Diperbarui!', null);
    }
}
