<?php

namespace App\Http\Controllers\Api\v1\Histories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserContentHistories;

class getViewedContents extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            UserContentHistories::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        $data = UserContentHistories::with('content.contentGroup')->where(UserContentHistories::ID_USER, $request[UserContentHistories::ID_USER])->get();

        return APIresponse(true, 'Data Histori Konten Berhasil Didapatkan!', $data);
    }
}
