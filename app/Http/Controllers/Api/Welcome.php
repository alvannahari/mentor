<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Welcome extends Controller {
    
    function __invoke() {
        return response([
            'App'   => 'Api Incar',
            'Versi' => '1.0.0'
        ]);
    }
}
