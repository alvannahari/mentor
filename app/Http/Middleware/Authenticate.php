<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    protected $guard = false;

    public function handle($request, Closure $next, ...$guards) {
        if ($guards[0] == 'api') {
            $this->guard = true;
            $this->authenticate($request, $guards);
            return $next($request);
        } else {
            foreach(array_keys(config('auth.guards')) as $guard){
                if(auth()->guard($guard)->check()) {
                    $this->authenticate($request, $guards);
                    return $next($request);
                    break;
                };
            }
            return redirect()->route('login');
        }
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request) {
        if ($this->guard) {
            return route('error');
        } else {
            return route('no_access');
            // if (! $request->expectsJson()) {
            // }
        }
    }
}
