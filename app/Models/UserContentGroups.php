<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentGroups extends Model {
    
    const ID = 'id';
    const ID_USER = 'user_id';
    const ID_GROUP = 'content_group_id';

    protected $guarded = [];

}
