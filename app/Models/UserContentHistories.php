<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentHistories extends Model {
    
    const ID = 'id';
    const ID_USER = 'user_id';
    const ID_CONTENT = 'content_id';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';

    protected $guarded = [];

    function content() {
        return $this->belongsTo('App\Models\Content');
    }

    function user() {
        return $this->belongsTo('App\Models\User');
    }
}
