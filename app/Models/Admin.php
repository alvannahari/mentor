<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable {
    
    use Notifiable;
    // use Notifiable;

    const USERNAME = 'username';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const PASSWORD = 'password';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD];
}
