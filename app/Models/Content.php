<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Content extends Model {
    
    const ID = 'id';
    const ID_GROUP = 'content_group_id';
    const ID_SUPER = 'superuser_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const TYPE = 'type';
    const URL = 'url';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';

    protected $guarded = [];

    function delete() {
        $this->comment()->delete();
        $this->history()->delete();

        return parent::delete();
    }

    function contentGroup() {
        return $this->belongsTo('App\Models\ContentGroup');
    }

    function comment() {
        return $this->hasMany('App\Models\UserContentComment');
    }

    function history() {
        return $this->hasMany('App\Models\UserContentHistories');
    }
}
