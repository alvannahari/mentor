<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentComment extends Model {
    
    const ID = 'id';
    const ID_USER = 'user_id';
    const ID_CONTENT = 'content_id';
    const COMMENT = 'comment';
    
    protected $guarded = [];

    function content() {
        return $this->belongsTo('App\Models\Content');
    }

    function user() {
        return $this->belongsTo('App\Models\User');
    }
}
