<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentGroup extends Model {
    
    const ID = 'id';
    const ID_SUPER = 'superuser_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';

    protected $guarded = [];

    function delete() {
        $this->content()->delete();

        return parent::delete();
    }

    function content() {
        return $this->hasMany('App\Models\Content');
    }
    
    function users() {
        return $this->belongsToMany('App\Models\User', 'user_content_groups');
    }
}
