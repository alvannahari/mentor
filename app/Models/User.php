<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {

    use HasApiTokens, Notifiable;
    
    const ID = 'id';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const PHONE = 'phone_number';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const GENDER = 'gender';
    const REM_TOKEN = 'remember_token';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD, SELF::REM_TOKEN];

    function delete() {
        $this->comment()->delete();
        $this->history()->delete();

        return parent::delete();
    }

    function contentGroups() {
        return $this->belongsToMany('App\Models\ContentGroup', 'user_content_groups');
    }

    function comment() {
        return $this->hasMany('App\Models\UserContentComment');
    }

    function history() {
        return $this->hasMany('App\Models\UserContentHistories');
    }


}
