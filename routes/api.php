<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('error', function () {
    return response([
        'status'    => false,
        'message'   => 'Request Unauthorized'
    ], 401);
})->name('error');

Route::get('/', function() {
    return response([
        'app' => 'API_Mentor',
        'version' => 'v1.0'
    ], 200);
});

Route::post('login', 'Api\v1\login');
Route::post('register', 'Api\v1\register');

Route::middleware('auth:api')->group( function() {
    Route::prefix('users')->group( function () {
        Route::post('updateUser', 'Api\v1\Users\updateUser');
        Route::get('getDetailUser', 'Api\v1\Users\getDetailUser');
    });
    
    Route::prefix('contents')->group( function () {
        Route::get('getUserAuthorizedGroups', 'Api\v1\Contents\getUserAuthorizedGroups');
        Route::get('getUserAuthorizedContents', 'Api\v1\Contents\getUserAuthorizedContents');
        Route::get('getContentsByGroup', 'Api\v1\Contents\getContentsByGroup');
        Route::get('getDetailContent', 'Api\v1\Contents\getDetailContent');
        Route::get('getContentComments', 'Api\v1\Contents\getContentComments');
        Route::post('submitComment', 'Api\v1\Contents\submitComment');
    });
    
    Route::prefix('histories')->group( function() {
        Route::post('submitViewedContent', 'Api\v1\Histories\submitViewedContent');
        Route::get('getViewedContents', 'Api\v1\Histories\getViewedContents');
    });
});


