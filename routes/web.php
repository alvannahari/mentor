<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('error', function () {
    return response([
        'status'    => false,
        'message'   => 'Akses Ditolak!'
    ], 401);
})->name('no_access');

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login');

Route::group(['middleware' => ['auth:admin,super']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/logout', 'AuthController@logout');

    Route::get('/contents', 'ContentGroupsController@index');
    Route::get('/getAllContentGroups', 'ContentGroupsController@getAllContentGroups');
    Route::post('/contents', 'ContentGroupsController@store');
    Route::get('/contents/{contents}', 'ContentGroupsController@show');
    Route::get('/contents/{contentGroup}/delete', 'ContentGroupsController@destroy');
    Route::patch('/contents/{contentGroup}', 'ContentGroupsController@update');
    Route::post('/accessContents/{contentGroup}', 'ContentGroupsController@updateAccessContents');
    
    Route::get('/content/{content}', 'ContentsController@show');
    Route::post('/content/{contentGroup}', 'ContentsController@store');
    Route::get('/content/{content}/delete', 'ContentsController@destroy');
    Route::patch('/content/{content}', 'ContentsController@update');
    
    Route::post('/comment/{content}', 'CommentController@store');
    Route::get('/comment/{userContentComment}/delete', 'CommentController@destroy');

    Route::get('/user', 'UsersController@index');
    Route::get('/getAllUsers', 'UsersController@getAllUsers');
    Route::post('/user', 'UsersController@store');
    Route::get('/user/{users}', 'UsersController@show');
    Route::get('/user/{user}/delete', 'UsersController@destroy');
    Route::patch('/user/{user}', 'UsersController@update');
    Route::post('/accessUsers/{user}', 'UsersController@updateAccessUsers');
});

Route::group(['middleware' => ['auth:super']], function () {
    Route::get('/admin', 'AdminsController@index');
    Route::get('/getAllAdmins', 'AdminsController@getAllAdmins');
    Route::post('/admin', 'AdminsController@store');
    Route::delete('/admin', 'AdminsController@destroy');
    Route::patch('/admin', 'AdminsController@update');
});